import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FSPViewRegister extends JPanel  implements ActionListener, PropertyChangeListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static JButton register = new JButton("Register");
	FSProtokollModel model;
	
	JTextField name = new JTextField("",15);
	JTextField number = new JTextField("",10);
	boolean registerButtonChecked = false;

	public FSPViewRegister(FSProtokollModel model)
	{
		model.addPropertyChangeListener(this);
	
		this.model = model;
		setBackground(Color.lightGray);
		
		Box box = Box.createVerticalBox();
		box.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 50));
		JLabel label1 = new JLabel("Name: ");
		label1.setAlignmentY(TOP_ALIGNMENT);
		box.add(label1);
		box.add(Box.createVerticalStrut(5));
		name.setAlignmentX(LEFT_ALIGNMENT);
		box.add(name);
		box.add(Box.createVerticalStrut(20));
		box.add(new JLabel("Password: "));
		box.add(Box.createVerticalStrut(5));
		number.setAlignmentX(LEFT_ALIGNMENT);
		box.add(number);
		box.add(Box.createVerticalStrut(15));
		register.addActionListener(this);
		register.setAlignmentX(LEFT_ALIGNMENT);
		box.add(register);
		add(box);		
		
	}

	
	/*
	@Override
	
	public void propertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
		String pn = evt.getPropertyName();
		switch(pn) {
		case FSProtokollModel.OUTPUT_CHANGE:
			number_output.setText(e.getNewValue()+"");
			break;
		default: 
			throw new IllegalArgumentException("Unknown property name: "+pn);
		}
	}
	*/
	public void checkInput() throws Exception{
		//FSPViewRegister fspTemp;
		
			try {
				if(name.getText().equals("") || number.getText().equals(""))
				{
					throw new Exceptions("Sie haben keinen Wert fuer Name oder Passwort eingegeben!");
				}
				
				if(!(Byte.valueOf(number.getText()) >= 1 && Byte.valueOf(number.getText()) <= 101))
				{
					throw new Exceptions("Der Wert der Nummer muss zwischen 1 und 101 sein");
				}
				
				
				model.setRegName(name.getText());
				model.setRegNumber(Byte.valueOf(number.getText()));
				JOptionPane.showMessageDialog(this,
						"Sie sind erfolgreich registeriet!","Information",JOptionPane.OK_OPTION);
			} 
			catch (NumberFormatException nfe) {
				JOptionPane.showMessageDialog(this,
						nfe.getMessage(),"Eingabefehler",JOptionPane.ERROR_MESSAGE);
			}
			catch (Exceptions e)
			{
				JOptionPane.showMessageDialog(this,
						e.getMessage(),"Eingabefehler",JOptionPane.ERROR_MESSAGE);
			}
		
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == register)
			try {
				checkInput();
				//registerButtonChecked  = true;
				
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
		
	}
	
}
