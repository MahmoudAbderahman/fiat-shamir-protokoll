

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

public class FSProtokollFrame extends JFrame  {
	private static final long serialVersionUID = 1L;
	public FSProtokollFrame()
	{
		super("Fiat Shamir Protokoll");
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e){
				System.exit(0);
			}
		});
		JTabbedPane tp = new JTabbedPane();
	    tp.setBounds(50,50,200,200);  
		FSProtokollModel model = new FSProtokollModel();

	    FSPViewRegister reg = new FSPViewRegister(model);
	    
	    FSProtokollViewLogin login = new FSProtokollViewLogin(model);
	    
	    tp.add("Register",reg);
	    tp.add("Login", login);
		getContentPane().add(tp);
		setSize(1000,500);
		pack();
	}
	
	

	public static void main(String[] args) {
		FSProtokollFrame ff = new FSProtokollFrame();
		ff.setVisible(true);
	}
	 
	
}
