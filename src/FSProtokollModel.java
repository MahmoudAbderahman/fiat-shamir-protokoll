import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.JOptionPane;

public class FSProtokollModel {

	String regName, logName;
	byte  regNumber, logNumber;
	static final String LOGINERLAUBT = "login erlaubt";
	static final String LOGINABGELEHNT = "login abgelehnt";

	private PropertyChangeSupport pcs = new PropertyChangeSupport(this);



	public void addPropertyChangeListener(PropertyChangeListener l) {
		// TODO Auto-generated method stub
		pcs.addPropertyChangeListener(l);

	}

	public void setRegName(String name) {
		// TODO Auto-generated method stub
		regName = name;
		
	}
	public void setRegNumber(byte number) {
		// TODO Auto-generated method stub
		regNumber = number;
	}
	public void setLogName(String name)
	{
		logName = name;
	}
	public void setLogNum(byte number)
	{
		logNumber = number;
	}

	public void test() {
		// TODO Auto-generated method stub
		if(regName.equals(logName) && regNumber == logNumber)
		{
			pcs.firePropertyChange(LOGINERLAUBT,null,true);
		}
		else
		{
			pcs.firePropertyChange(LOGINABGELEHNT,null,true);

		}
	}
}
