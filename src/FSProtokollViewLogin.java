import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.*;
public class FSProtokollViewLogin extends JPanel implements ActionListener, PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	FSProtokollModel model;
	
	JButton login = new JButton("Login");
	JTextField name = new JTextField("",15);
	JTextField number = new JTextField("",10);

	
	public FSProtokollViewLogin(FSProtokollModel model) {
		// TODO Auto-generated constructor stub
		model.addPropertyChangeListener(this);

		this.model = model;
		setBackground(Color.lightGray);
		
		Box box = Box.createVerticalBox();
		box.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 50));
		JLabel label1 = new JLabel("Name: ");
		label1.setAlignmentY(TOP_ALIGNMENT);
		box.add(label1);
		box.add(Box.createVerticalStrut(5));
		name.setAlignmentX(LEFT_ALIGNMENT);
		box.add(name);
		box.add(Box.createVerticalStrut(20));
		box.add(new JLabel("Password: "));
		box.add(Box.createVerticalStrut(5));
		number.setAlignmentX(LEFT_ALIGNMENT);
		box.add(number);
		box.add(Box.createVerticalStrut(15));
		login.addActionListener(this);
		login.setAlignmentX(LEFT_ALIGNMENT);
		box.add(login);
		add(box);		
	}
	public void checkInput() throws Exception{
		//FSPViewRegister fspTemp;
		
			try {
				if(name.getText().equals("") || number.getText().equals(""))
				{
					throw new Exceptions("Sie haben keinen Wert fuer Name oder Passwort eingegeben!");
				}
				
				
				model.setLogName(name.getText());
				model.setLogNum(Byte.valueOf(number.getText()));
				model.test();
				//model.ggt();
				//FSPViewRegister.temp_model.setRegName(FSPViewRegister.name.getText());
				//FSPViewRegister.temp_model.setRegNumber(Byte.valueOf(FSPViewRegister.number.getText()));
				
			} 
			catch (NumberFormatException nfe) {
				JOptionPane.showMessageDialog(this,
						nfe.getMessage(),"Eingabefehler",JOptionPane.ERROR_MESSAGE);
			}
			catch (Exceptions e)
			{
				JOptionPane.showMessageDialog(this,
						e.getMessage(),"Eingabefehler",JOptionPane.ERROR_MESSAGE);
			}
		
		
		
	}
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		// The switch is not strictly necessary in this example as there is
				// only one property "ggt". Do it anyway to show the general pattern.
				String pn = evt.getPropertyName();
				switch(pn) {
				case FSProtokollModel.LOGINERLAUBT:
					JOptionPane.showMessageDialog(this, "Zugriff ist erlaubt", "Information", JOptionPane.OK_OPTION);
					break;
				case FSProtokollModel.LOGINABGELEHNT:
					JOptionPane.showMessageDialog(this, "Zugriff ist abgelehnt", "Information", JOptionPane.OK_OPTION);
					break;
				default: 
					throw new IllegalArgumentException("Unknown property name: "+pn);
				}
		
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == login)
			try {
				checkInput();
				
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	}
	
}
